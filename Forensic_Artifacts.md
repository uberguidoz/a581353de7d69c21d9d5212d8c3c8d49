## *This is a list of forensic artifacts that can be used by DFIR community to perform cyber investigations*.

## *USB Devices Log Files:*
  * __XP__  - c:\windows\setupapi.log
  * __W7+__ - c:\windows\inf\setupapi.dev.log

## *Recycle Bin:*
 * c:\$Recycle.Bin\*
 * c:\Recycler\*

## *Web-Based Enterprise Management (WBEM):*
 * c:\windows\system32\wbem\Repository*

## *Windows Index Search:*
 * c:\programdata\Microsoft\search\Data\Application\windows\windows.edb

## *System Resource Usage Monitor Data:*
 * c:\windows\system32\SRU

## *Thumbnail Cache DB:*
 * c:\Users\*\AppData\Local\Microsoft\Windows\Explorer\thumbcache_*.db

## *Skype Logs:*
 * __W7+__ - c:\Users\*\AppData\Local\Packages\Microsoft.SkypeApp_*\LocalState\*\main.db
 * __XP__  - c:\Documents and Settings\*\Application Data\Skype\*\main.db

## *Scheduled Tasks:*
 * c:\windows\tasks\*.job
 * c:\windows\system32\schedLgU.txt
 * c:\windows\system32\tasks

## *System and user Reg hives:*
  * __XP__  - c:\Documents and Settings\*\ntuser.dat
  * __W7+__ - c:\users\*\ntuser.dat
  * c:\users\*\ntuser.dat.LOG*
  * c:\users\*\appdata\local\microsoft\windows\Usrclass.dat
  * c:\users\*\appdata\local\microsoft\windows\Usrclass.dat.Log*
  * c:\windows\system32\config\SAM.LOG*
  * c:\windows\system32\config\SECURITY.LOG*
  * c:\windows\system32\config\SOFTWARE.LOG*
  * c:\windows\system32\config\SYSTEM.LOG*
  * c:\windows\system32\config\SAM
  * c:\windows\system32\config\SECURITY
  * c:\windows\system32\config\SOFTWARE
  * c:\windows\system32\config\SYSTEM
  * c:\windows\system32\config\RegBack\*.LOG*
  * c:\windows\system32\config\RegBack\SAM
  * c:\windows\system32\config\RegBack\SECURITY
  * c:\windows\system32\config\RegBack\SOFTWARE
  * c:\windows\system32\config\RegBack\SYSTEM
  * c:\windows\system32\config\RegBack\SYSTEM1

## *Outlook PST and OST Files:*
  * c:\Documents and Settings\*\Local Settings\Application Data\Microsoft\Outlook\*.pst
  * c:\Documents and Settings\*\Local Settings\Application Data\Microsoft\Outlook\*.ost
  * c:\users\*\Appdata\Local\Microsoft\Outlook\*.pst
  * c:\users\*\Appdata\Local\Microsoft\Outlook\*.ost

## *Link Files and Jump Lists:*
  * c:\users\*\Appdata\Roaming\Microsoft\Windows\Recent
  * c:\Documents and Settings\*\Recent
  * c:\Documents and Settings\*\Desktop\*.lnk
  * c:\users\*\Desktop\*.lnk
  * c:\users\*\Appdata\Local\ConnectedDevicesPlatform\*\*.db

## *Internet Explorer:*
  * c:\Documents and Settings\*\Local Settings\History\History.IE5\index.dat
  * c:\Documents and Settings\*\Local Settings\History\History.IE5\*\index.dat
  * c:\Documents and Settings\*\Local Settings\Temporary Internet Files\Content.IE5\\index.dat
  * c:\Documents and Settings\*\Cookies\index.dat
  * c:\Documents and Settings\*\Local Settings\Application Data\Microsoft\Internet Explorer\Userdata\index.dat
  * c:\Documents and Settings\*\Local Settings\Application Data\Microsoft\Office\Recent\index.dat
  * c:\Users\*\Appdata\Roaming\Microsoft\Office\Recent\index.dat
  * c:\Users\*\Appdata\Local\Microsoft\Windows\Webcache\WebCacheV01.dat

## *FireFox:*
  * c:\users\*\Appdata\Roaming\Mozilla\Firefox\Profiles\*.default\places.sqlite*
  * c:\users\*\Appdata\Roaming\Mozilla\Firefox\Profiles\*.default\downloads.sqlite*
  * c:\users\*\Appdata\Roaming\Mozilla\Firefox\Profiles\*.default\formhistory.sqlite*
  * c:\users\*\Appdata\Roaming\Mozilla\Firefox\Profiles\*.default\cookies.sqlite*
  * c:\users\*\Appdata\Roaming\Mozilla\Firefox\Profiles\*.default\signons.sqlite*
  * c:\users\*\Appdata\Roaming\Mozilla\Firefox\Profiles\*.default\webappstore.sqlite*
  * c:\users\*\Appdata\Roaming\Mozilla\Firefox\Profiles\*.default\favicons.sqlite*
  * c:\users\*\Appdata\Roaming\Mozilla\Firefox\Profiles\*.default\addons.sqlite*
  * c:\users\*\Appdata\Roaming\Mozilla\Firefox\Profiles\*.default\search.sqlite*
  * c:\Documents and Settings\*\Application Data\Mozilla\Firefox\Profiles\*.default\places.sqlite*
  * c:\Documents and Settings\*\Application Data\Mozilla\Firefox\Profiles\*.default\downloads.sqlite*
  * c:\Documents and Settings\*\Application Data\Mozilla\Firefox\Profiles\*.default\formhistory.sqlite*
  * c:\Documents and Settings\*\Application Data\Mozilla\Firefox\Profiles\*.default\cookies.sqlite*
  * c:\Documents and Settings\*\Application Data\Mozilla\Firefox\Profiles\*.default\signons.sqlite*
  * c:\Documents and Settings\*\Application Data\Mozilla\Firefox\Profiles\*.default\webappstore.sqlite*
  * c:\Documents and Settings\*\Application Data\Mozilla\Firefox\Profiles\*.default\favicons.sqlite*
  * c:\Documents and Settings\*\Application Data\Mozilla\Firefox\Profiles\*.default\addons.sqlite*
  * c:\Documents and Settings\*\Application Data\Mozilla\Firefox\Profiles\*.default\search.sqlite*

## *Chrome:*
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Bookmarks*
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Cookies*
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Current Session
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Current Tabs
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Favicons*
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\History*
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Last Session
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Last Tabs
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Preferences
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Shortcuts*
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Top Sites*
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Visited Links
  * c:\Documents and Settings\*\Local Settings\Application Data\Google\Chrome\User Data\Default\Web Data*
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Bookmarks*
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Cookies*
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Current Session
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Current Tabs
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Favicons*
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\History*
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Last Session
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Last Tabs
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Preferences
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Shortcuts*
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Top Sites*
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Visited Links
  * c:\Users\*\AppData\Local\Google\Chrome\User Data\Default\Web Data*

## *File System Metadata:*
  * c:\$MFT
  * c:\$LogFile
  * c:\$Extend\$UsnJrnl:$J

## *Execution Artifacts:*
  * c:\windows\prefetch
  * c:\windows\appcompat\programs\RecentFileCache.bcf
  * c:\windows\appcompat\programs\Amcache.hve
  * c:\windows\appcompat\programs\Amcache.hve.LOG*

## *Event Logs:*
  * c:\windows\system32\config\*.evt
  * c:\windows\system32\winevt\logs\*.evtx

## *Memory Artifacts:*
  * c:\hiberfile.sys
  * c:\pagefile.sys
  * c:\swapfile.sys
  * c:\windows\memory.dmp

## *Event Trace Logs:*
  * c:\windows\system32\WDI\LogFiles\*.etl*
  * c:\windows\system32\WDI\{*
  * c:\windows\system32\LogFiles\WMI\*
  * c:\windows\system32\SleepStudy*
  * c:\programdata\microsoft\windows\PowerEfficiency Diagnostics\energy-ntkl.etl